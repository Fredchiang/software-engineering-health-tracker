

package healthtracker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author erk13pxa
 */
public class Setgoal {
    
    
    private DBconnection db = new DBconnection();
    private Connection conn = null;
  
    public boolean add(){
        if (null == db)
            return false;
        conn = db.getConn();
        if (null== conn)
            return false;
        PreparedStatement pstmt;
      
        try{
          
            pstmt = conn.prepareStatement("insert into account(goal) values(?)");
            pstmt.setString(1,"goal");
            return pstmt.executeUpdate() != 0;
        }catch (SQLException e){
            e.printStackTrace();
    }
        return false;
  }
}
