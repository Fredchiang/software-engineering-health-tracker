

package healthtracker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RecordDetail {
    
    private DBconnection db = new DBconnection();
    private Connection conn = null;
  
    public boolean add(){
        if (null == db)
            return false;
        conn = db.getConn();
        if (null== conn)
            return false;
        PreparedStatement pstmt;
      
        try{
          
            pstmt = conn.prepareStatement("insert into account(name, username, email, height, weight) values(?, ?, ?, ?, ?)");
            pstmt.setString(1,"name");
            pstmt.setString(2,"username");
            pstmt.setString(3,"email");
            pstmt.setString(4,"height");
            pstmt.setString(5,"weight");
            return pstmt.executeUpdate() != 0;
        }catch (SQLException e){
            e.printStackTrace();
    }
        return false;
  }
}
