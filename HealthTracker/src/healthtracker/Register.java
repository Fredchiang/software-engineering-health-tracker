package healthtracker;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Register implements WindowListener {

    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    private JFrame frm = new JFrame();
    private JPanel pnl = new JPanel();

    private JButton submit = new JButton("Submit");
    private JButton cancel = new JButton("Cancel");

    private JTextField username = new JTextField(20);
    private JPasswordField password = new JPasswordField(20);
    private JPasswordField confirmPassword = new JPasswordField(20);

    public Register(String title) {

        final Account a = new Account();
        final AccountController ac = new AccountController(a);

        JPanel inf = new JPanel();
        JButton buttons = new JButton("Create");
        inf.add(new JLabel("UserName :"));
        inf.add(username);
        inf.add(new JLabel("Password"));
        inf.add(password);
        inf.add(new JLabel("Confirm Password :"));
        inf.add(confirmPassword);
        inf.add(buttons);
        buttons.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                ac.email = username.getText();
                ac.password = password.getText();
                ac.confirmPassword = confirmPassword.getText();
                
                ac.actionPerformed(e);
                username.setText("");
                password.setText("");
                confirmPassword.setText("");

            }
        });
        inf.setPreferredSize(new Dimension(640, 480));

        frm.add(inf, BorderLayout.EAST);
        frm.add(buttons);

        frm.setLocation(350, 200);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // EDIT
        frm.setResizable(true);
        frm.pack();
        frm.setVisible(true);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                Register HT = new Register("Register");
            }
        });
    }

}
