package healthtracker;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class User {

    private JFrame frm = new JFrame();
    private JPanel pnl = new JPanel();
    private JButton logout = new JButton("Logout");
    private JButton viewProfile = new JButton("View Profile");
    private JButton viewGoat = new JButton("View Goat");
    private JButton setGoat = new JButton("Set Goat");
    private JButton group = new JButton("Group");

    public User() {
        JPanel inf = new JPanel();
        JPanel buttons = new JPanel();
        
        buttons.add(viewProfile);
        buttons.add(viewGoat);
        buttons.add(setGoat);
        buttons.add(group);

        inf.setPreferredSize(new Dimension(900, 640));

        frm.add(inf, BorderLayout.EAST);
        frm.add(buttons,BorderLayout.CENTER);
        frm.add(logout, BorderLayout.SOUTH);
        inf.setLocation(0, 0);

        frm.setLocation(200, 130);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // EDIT
        frm.setResizable(false);
        frm.pack();
        frm.setVisible(true);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                User HT = new User();
            }
        });
    }
}
