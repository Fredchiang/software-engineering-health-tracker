package healthtracker;
import java.util.Calendar;
import java.util.Date;

import java.util.Calendar.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.*;
import javax.swing.JComponent;



public class ViewGoal {

    private JFrame frm = new JFrame();
    private JPanel pnl = new JPanel();
    private JButton homepage = new JButton("Go to Home page");
    private JButton logout = new JButton("Logout");
    private JButton submit = new JButton("Submit");
    private JButton cancel = new JButton("Cancel");
    private JTextField name = new JTextField(20);
    private JTextField username = new JTextField(20);
    private JTextField email = new JTextField(20);
    private JTextField height = new JTextField(10);
    private JTextField weight = new JTextField(10);
    private JTextArea text1 = new JTextArea(50, 30);
    private JTextArea text2 = new JTextArea(50, 30);
    private JLabel label1 = new JLabel("View Goal :");
    public Date date = new Date();
    Calendar c;
    
    
    public ViewGoal() {
        
        JPanel inf = new JPanel();
        JPanel buttons = new JPanel();
        inf.add(label1);
        label1.setLocation(200, 330);
        
        
        inf.add(text2);
        
        buttons.add(submit);
        buttons.add(cancel);

        inf.setPreferredSize(new Dimension(900, 640));

        frm.add(inf, BorderLayout.CENTER);
        //frm.add(buttons);
        
        frm.add(logout, BorderLayout.SOUTH);
        frm.add(homepage, BorderLayout.NORTH);
        inf.setLocation(0, 0);

        frm.setLocation(200, 130);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // EDIT
        frm.setResizable(false);
        frm.pack();
        frm.setVisible(true);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                ViewGoal HT = new ViewGoal();
            }
        });
    }
}
