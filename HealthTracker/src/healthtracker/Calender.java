package healthtracker;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

public class Calender extends JFrame implements ActionListener {
 private static final ArrayList months;
 static {
 months = new ArrayList(12);
 months.add("January");
 months.add("February");
 months.add("March");
 months.add("April");
 months.add("May");
 months.add("June");
 months.add("July");
 months.add("August");
 months.add("September");
 months.add("October ");
 months.add("November");
 months.add("December");
 };
 private GregorianCalendar cal;
 private JLabel label1;
 private JLabel label2;
 private JPanel dayPanel;
 private boolean ready;

 public Calender ( GregorianCalendar d) {
 cal = d;
 setLayout(new BorderLayout());
 JPanel panel1 = new JPanel();
 JPanel panel2 = new JPanel();
 JButton[] buttons = new JButton[4];

 panel2.add(buttons[0] = new JButton("<"));
 panel2.add(label1 =new JLabel(String.valueOf(months.get(cal.get(GregorianCalendar.MONTH))), JLabel.CENTER));
 panel2.add(buttons[1] = new JButton(">"));
	panel2.setBackground(Color.white);

 panel1.add(buttons[2] = new JButton("<<"));
 panel1.add(label2 = new JLabel(String.valueOf(cal.get(GregorianCalendar.YEAR)),JLabel.CENTER), BorderLayout.CENTER);
 panel1.add(buttons[3] = new JButton(">>"));
	panel1.setBackground(Color.white);
 for (int i=0; i<4; i++) {
 buttons[i].addActionListener(this);
	buttons[i].setBackground(Color.white);
	buttons[i].setForeground(Color.blue);
 }
 JPanel panel = new JPanel();
 panel.setBorder(BorderFactory.createEmptyBorder(5, 10, 0, 10));
 panel.add(panel2);
 panel.add(panel1);
	panel.setBackground(Color.white);
 dayPanel = new JPanel(new GridLayout(7, 7));
 updateDayGrid();
 add(panel, BorderLayout.NORTH);
 add(dayPanel, BorderLayout.CENTER);
 ready = false;
 pack();
 setVisible(true);
	}
 public GregorianCalendar getCal () {
 return cal;
 }
 public void actionPerformed (ActionEvent evt) {
 String label = ((JButton) evt.getSource()).getText();
 if (label.equals("<")) {
 int m = months.indexOf(label1.getText());
 m = prevMonth(m);
 label1.setText((String) months.get(m));
 updateDayGrid();
 }
 else if (label.equals(">")) {
 int m = months.indexOf(label1.getText());
 m = nextMonth(m);
 label1.setText((String) months.get(m));
 updateDayGrid();
 } 
else if (label.equals("<<")) {
 int y = 0;
 try {
 y = Integer.parseInt(label2.getText());
 }
 catch (NumberFormatException e) {
 System.err.println(e.toString());
 }
 label2.setText(String.valueOf(--y));
 updateDayGrid();
 }
 else if (label.equals(">>")) {
 int y = 0;
 try {
 y = Integer.parseInt(label2.getText());
 }
 catch(Exception e){}
 label2.setText(String.valueOf(++y));
 updateDayGrid();
 }
	else {
 int m = months.indexOf(label1.getText());
 int y = 0;
 int d = 0;
 try {
 y = Integer.parseInt(label2.getText());
 d = Integer.parseInt(label);
 }
 catch(Exception e){}
 cal = new GregorianCalendar(y, m, d);
 cal.setLenient(false);
 }
 }
 private void updateDayGrid () {
	dayPanel.removeAll();
 int m = months.indexOf(label1.getText());
 int y = 0;
 try {
 y = Integer.parseInt(label2.getText());
 }
 catch(Exception e){} 
 GregorianCalendar temp = new GregorianCalendar(y, m, 1);
 temp.setLenient(false);
 int offset = 0;
 switch(temp.get(GregorianCalendar.DAY_OF_WEEK)) {
	case GregorianCalendar.SUNDAY : offset = 0; break;
 case GregorianCalendar.MONDAY : offset = 1; break;
 case GregorianCalendar.TUESDAY : offset = 2; break;
 case GregorianCalendar.WEDNESDAY : offset = 3; break;
 case GregorianCalendar.THURSDAY : offset = 4; break;
 case GregorianCalendar.FRIDAY : offset = 5; break;
 case GregorianCalendar.SATURDAY : offset = 6; break;
 }
 String daynames[]={"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
 JLabel l[]=new JLabel[7];
 for(int i=0;i<7;i++){
	l[i]=new JLabel(daynames[i], JLabel.CENTER);
	dayPanel.add(l[i]);
	l[i].setForeground(Color.red);
	}
 for (int i=1; i<=offset; i++) {
 dayPanel.add(new JLabel(""));
 }
 JButton day;
 for (int i=1; i<=getLastDay(); i++) {
 dayPanel.add(day = new JButton(String.valueOf(i)));
 day.addActionListener(this);
	day.setForeground(Color.blue);
 day.setBackground(Color.white);
	dayPanel.setBackground(Color.white);
 if (i == cal.get(Calendar.DATE) &&
 m == cal.get(Calendar.MONTH) &&
 y == cal.get(Calendar.YEAR)) {
 day.setForeground(Color.red);
	}
 }
 for (int i=(offset+getLastDay()+1); i<=42; i++) {
 dayPanel.add(new JLabel(""));
 }
 repaint();
 }
 private int nextMonth (int month) {
 if (month == 11) {
 return(0);
 }
 return(++month);
 }
 private int prevMonth (int month) {
 if (month == 0) {
 return(11);
 }
 return(--month);
 }
 private int getLastDay () {
 int m = (months.indexOf(label1.getText()) + 1);
 int y = 0;
 try {
 y = Integer.parseInt(label2.getText());
 }
 catch(Exception e){}
 if ((m==4) || (m==6) || (m==9) || (m==11)) {
 return(30);
 }
 else if (m==2) {
 if (cal.isLeapYear(y)) {
 return(29);
 }
 return(28);
 }
 return(31);
 }
 public static void main(String[]args){
	GregorianCalendar cal=new GregorianCalendar();
	Calender e=new Calender(cal);
}
}