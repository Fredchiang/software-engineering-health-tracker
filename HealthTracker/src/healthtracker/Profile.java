package healthtracker;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.*;

public class Profile {

    private JFrame frm = new JFrame();
    private JPanel pnl = new JPanel();
    private JButton homepage = new JButton("Go to Home Page");
    private JButton logout = new JButton("Logout");
    private JButton submit = new JButton("Submit");
    private JButton cancel = new JButton("Cancel");
    private JTextField name = new JTextField(20);
    private JTextField username = new JTextField(20);
    private JTextField email = new JTextField(20);
    private JTextField height = new JTextField(10);
    private JTextField weight = new JTextField(10);
    
    public Profile() {
        JPanel inf = new JPanel();
        JPanel buttons = new JPanel();
        inf.add(new JLabel("FullName :"));
        inf.add(name);
        inf.add(new JLabel("UserName :"));
        inf.add(username);
        inf.add(new JLabel("Email :"));
        inf.add(email);
        inf.add(new JLabel("Height :"));
        inf.add(height);
        inf.add(new JLabel("Weight :"));
        inf.add(weight);
        buttons.add(submit);
        buttons.add(cancel);

        inf.setPreferredSize(new Dimension(900, 640));
        logout.setPreferredSize(new Dimension(20, 20));
        homepage.setPreferredSize(new Dimension(20, 20));

        frm.add(inf, BorderLayout.EAST);
        frm.add(buttons);
        frm.add(logout, BorderLayout.SOUTH);
        frm.add(homepage, BorderLayout.NORTH);
        inf.setLocation(0, 0);

        frm.setLocation(200, 130);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // EDIT
        frm.setResizable(false);
        frm.pack();
        frm.setVisible(true);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                Profile HT = new Profile();
            }
        });
    }
}
