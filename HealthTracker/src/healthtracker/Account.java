package healthtracker;

import com.sun.corba.se.impl.util.Version;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Account {

//define variables
    private String email;
    private String password;
    private String user_name;
    private String name;
    private Date dateCreated;

    public Account() {
        email = null;
        password = null;
    }

    public void createAccount(String newId, String newPassword, String newUser_name, String newName) throws FileNotFoundException, UnsupportedEncodingException {
        email = newId;
        password = newPassword;
        dateCreated = new java.util.Date();

        user_name = newUser_name;
        name = newName;

        Connection con = null;
        PreparedStatement pst = null;

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String dbpassword = "dbpassword";

        try {
            con = DriverManager.getConnection(url, user, dbpassword);
            // st = con.createStatement();
            String stm = "INSERT INTO account(email, password, user_name, name, datecreated) VALUES(?, ?, ? , ?, ?)";

            pst = con.prepareStatement(stm);
            pst.setString(1, email);
            pst.setString(2, password);
            pst.setString(3, user_name);
            pst.setString(4, name);
            pst.setString(5, dateCreated.toString());

            pst.executeUpdate();

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Version.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Version.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }

    public void setDateCreated(Date newDateCreated) {
        dateCreated = newDateCreated;
    }


}
