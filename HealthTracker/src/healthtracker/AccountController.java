package healthtracker;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class AccountController implements ActionListener {

    String email;
    String password;
    String fname;
    String sname;
    String confirmPassword;

    private Account theModel;

    public AccountController(Account theModel) {

        this.theModel = theModel;

    }

    public void setUsername(String username) {
        this.email = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        PasswordEncryption encrypter = new PasswordEncryption();

        if (!(password.equals(confirmPassword))) {
            JOptionPane.showMessageDialog(null, "Your Passwords do not match!", "Your Passwords do not match!", JOptionPane.INFORMATION_MESSAGE);
        } else if (password.length() < 6) {
            JOptionPane.showMessageDialog(null, "Your Passwords is too short!", "Your Passwords is too short", JOptionPane.INFORMATION_MESSAGE);
        } else {
            try {
                theModel.createAccount(email, encrypter.encrypt(password),fname,sname);

            } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                Logger.getLogger(AccountController.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
