package healthtracker;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Login {

    private JFrame frm = new JFrame();
    private JPanel pnl = new JPanel();
    private JButton mainpage = new JButton("Mainpage");
    
    private JButton submit = new JButton("Submit");
    private JButton clear = new JButton("Clear");
    private JButton cancel = new JButton("Cancel");
    private JTextField username = new JTextField(50);
    private JTextField password = new JTextField(50);
    private JButton register = new JButton("Register");

    public Login() {
        JPanel inf = new JPanel();
        JPanel buttons = new JPanel();
        inf.add(new JLabel("Username :"));
        inf.add(username);
        inf.add(new JLabel("Password :"));
        inf.add(password);
        buttons.add(submit);
        buttons.add(clear);
        buttons.add(cancel);

        inf.setPreferredSize(new Dimension(900, 640));

        frm.add(inf, BorderLayout.EAST);
        frm.add(buttons);
        frm.add(register, BorderLayout.SOUTH);
        inf.setLocation(0, 0);

        frm.setLocation(200, 130);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // EDIT
        frm.setResizable(false);
        frm.pack();
        frm.setVisible(true);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                Login HT = new Login();
            }
        });
    }
}
