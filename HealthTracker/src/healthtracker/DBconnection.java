

package healthtracker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author erk13pxa
 */
public class DBconnection {
	private static String username = "dbuser";
	private static String password = "dbpassword";
	private Connection conn = null;

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
		}
	}

	public DBconnection() {
		try {
			Class.forName("org.postgresql.Driver").newInstance();
			String url = "jdbc:postgresql://localhost:5432/postgres";
			conn = DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
		}
	}
}
