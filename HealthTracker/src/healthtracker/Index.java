package healthtracker;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Index {

    private JFrame frm = new JFrame();
    private JPanel pnl = new JPanel();
    private JButton login = new JButton("Login");
    private JButton register = new JButton("Register");

    public Index() {
        JPanel pnl = new JPanel();
        pnl.add(new JLabel("Welcome to our HealthTracker application."));
        pnl.add(new JTextArea());
        pnl.add(new JLabel("Please select the above button to log in or click on the bottom for registration."));
        /*pnl.add(new JButton("Button 1"));
         pnl.add(new JButton("Button 2"));
         pnl.add(new JButton("Button 3"));*/

        login.setPreferredSize(new Dimension(20, 40));
        pnl.setPreferredSize(new Dimension(900, 640));
        frm.add(pnl, BorderLayout.EAST);
        frm.add(login, BorderLayout.NORTH);
        frm.add(register, BorderLayout.SOUTH);
        pnl.setLocation(0, 0);

        frm.setLocation(200, 130);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // EDIT
        frm.setResizable(false);
        frm.pack();
        frm.setVisible(true);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                Index HT = new Index();
            }
        });
    }
}
