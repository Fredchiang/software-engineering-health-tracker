package healthtracker;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.*;

public class Group {

    private JFrame frm = new JFrame();
    private JPanel pnl = new JPanel();
    private JButton homepage = new JButton("Go to Home Page");
    private JButton logout = new JButton("Logout");
    private JButton edit = new JButton("Edit");
    private JButton cancel = new JButton("Cancel");
    private JTextArea groupname = new JTextArea("YOYOY",20,20);
    private JTextField member = new JTextField(20);
    private JTextField groupgoat = new JTextField("GOAT!!!GOAT!!!GOAT!!!",20);
    private DefaultComboBoxModel memberList = new DefaultComboBoxModel();

    public Group() {
        JPanel inf = new JPanel();
        JPanel buttons = new JPanel();
        inf.add(new JLabel("Group Name :"));
        inf.add(groupname);
        groupname.setEditable(false);
        inf.add(new JLabel("Group member :"));
        memberList.addElement("AAA");
        memberList.addElement("BBB");
        memberList.addElement("C");
        logout.setPreferredSize(new Dimension(20, 20));
        homepage.setPreferredSize(new Dimension(20, 20));
        //DefaultComboBoxModel memberList = new DefaultComboBoxModel();
        JComboBox memberlist = new JComboBox(memberList);
        inf.add(memberlist);
        inf.add(new JLabel("Group Goat :"));
        inf.add(groupgoat);
        groupgoat.setEditable(false);
        buttons.add(edit);
        buttons.add(cancel);

        inf.setPreferredSize(new Dimension(900, 640));

        frm.add(inf, BorderLayout.EAST);
        frm.add(buttons);
        frm.add(logout, BorderLayout.SOUTH);
        frm.add(homepage, BorderLayout.NORTH);
        inf.setLocation(0, 0);

        frm.setLocation(200, 130);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // EDIT
        frm.setResizable(false);
        frm.pack();
        frm.setVisible(true);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                Group HT = new Group();
            }
        });
    }
}
