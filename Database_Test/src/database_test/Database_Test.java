package database_test;

import java.sql.*;

public class Database_Test {

    public static void main(String[] args) {
        
        String em = "";
        
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://127.0.0.1/studentdb";
            con = DriverManager.getConnection(url, "student", "dbpassword");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(2);
        }
        System.out.println("Database open");
       try{ 
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT email FROM account");
        em = rs.getString("email");
        rs.close();
       } catch(SQLException se){
           System.err.println("Threw exception");
           System.err.println(se.getMessage());
       }
       
        System.out.println(em);
       
    }
       
}


